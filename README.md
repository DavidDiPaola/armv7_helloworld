# armv7_helloworld
Bare metal QEMU ARMv7 example

## references
  - `QEMU:vexpress.c` -- qemu-X.Y.Z/hw/arm/vexpress.c
  - `DDI0183` -- ARM PL011 datasheet

2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)

